||
||Opcode 1 adds together numbers read from two positions and stores the result in a third position.
||The three integers immediately after the opcode tell you these three positions -
||the first two indicate the positions from which you should read the input values,
||and the third indicates the position at which the output should be stored.

%include "../utils"

op1 list pos = list!(list!(pos + 1)) + list!(list!(pos + 2))
op2 list pos = list!(list!(pos + 1)) * list!(list!(pos + 2))

generator position result = result, if equal (result!position) 99
                          = generator (position + 4) (inject result (result!(position + 3)) (op1 result position)), if equal (result!position) 1
                          = generator (position + 4) (inject result (result!(position + 3)) (op2 result position)), if equal (result!position) 2
                          = error "woops", otherwise

test1 = ([1,0,0,0,99], [2,0,0,0,99])
test2 = ([2,3,0,3,99 ], [2,3,0,6,99])
test3 = ([2,4,4,5,99,0], [2,4,4,5,99,9801])
test4 = ([1,1,1,4,99,5,6,0,99], [30,1,1,4,2,5,6,0,99])

tests = [test1, test2, test3, test4]

runTest (expected, actual) = assert (generator 0 expected) actual "Assertion failed"

input = read "input.txt"
trimmed_input = takewhile (not_equal '\n') (dropwhile (equal '\n') input)
numerical_input =  map to_number (split trimmed_input ',')


generate_input puzzle (noun, verb) = (inject (inject puzzle 1 noun) 2 verb)

first_element_equals value list = (equal (list!0) value)

noun_verb_combos = [ (noun, verb) | noun,verb<-[0..99] ]

filter_fn value (noun, verb) = equal ((generator 0 (inject (inject numerical_input 1 noun) 2 verb))!0) value


find_matching_combo value combos = combos!idx
                                   where
                                   idx = find_index (filter_fn value) combos

main = (show (find_matching_combo 19690720 noun_verb_combos))

