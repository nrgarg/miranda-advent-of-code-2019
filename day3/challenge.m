%include "../utils"

input = read "input.txt"
result = map ((converse split) ',') (split input '\n')
first_wire = result!0
second_wire = result!1

|| 1. Generate a list of coordinates for each wire i.e. (x,y)
|| 2. Iterate through second wire, find all line segements that intersect with first wire
|| 6. Foldl through result set from #2, searching for smallest distance

manhattan_distance (x1, y1) (x2, y2) = (abs (x1 - x2)) + (abs (y1 - y2))


move_coordinate 'R' d (x, y) = (x + d, y)
move_coordinate 'U' d (x, y) = (x, y + d)
move_coordinate 'L' d (x, y) = (x - d, y)
move_coordinate 'D' d (x, y) = (x, y - d)

generate_coordinate :: [char] -> (num, num) -> (num, num)
generate_coordinate (a:xs) (x, y) = move_coordinate a (numval xs) (x, y)

generate_coordinates directions = foldl op [(0, 0)] directions
                                  where
                                  op current_coordinates next = current_coordinates ++ [generate_coordinate next (last current_coordinates)]


generate_line_segments (coordinate1:coordinate2:coordinates) = foldl op [(coordinate1, coordinate2)] coordinates
                                                               where
                                                               op segments rhs = segments ++ [(last_coord, rhs)]
                                                                                 where
                                                                                 (ignore, last_coord) = last segments

|| If both lines are horizontal with same y value, check if x coordinates overlap
intersection_possible (x1, y1) (x2, y1) (x3, y1) (x4, y1) = or [(x1 <= x3 <= x2), (x1 <= x4 <= x2)]
|| If both lines are vertical with same x value, check if  y coordinates overlap
intersection_possible (x1, y1) (x1, y2) (x1, y3) (x1, y4) = or [(y1 <= y3 <= y2), (y1 <= y4 <= y4)]
|| If first line is horizontal and second line is vertical
intersection_possible (x1, y1) (x2, y1) (x3, y2) (x3, y3) = and [or [(x1 <= x3 <= x2), (x2 <= x3 <= x1)], or [(y2 <= y1 <= y3), (y3 <= y1 <= y2)]]
|| If first line is vertical and second line is horizontal
intersection_possible (x1, y1) (x1, y2) (x2, y3) (x3, y3) = intersection_possible (x2, y3) (x3, y3) (x1, y1) (x1, y2)|| and [(x2 <= x1 <= x3), (y1 <= y3 <= y2)]

intersection_possible (x1, y1) (x2, y2) (x3, y3) (x4, y4) = False

|| If both lines are horizontal with same y value
get_intersection_coordinate (x1, y1) (x2, y1) (x3, y1) (x4, y1) = (x, y1)
                                                                  where
                                                                  x = x3, if (x1 <= x3 <= x2)
                                                                    = x4, otherwise
|| If both lines are vertical with same x value
get_intersection_coordinate (x1, y1) (x1, y2) (x1, y3) (x1, y4) = (x1, y)
                                                                  where
                                                                  y = y3, if (y1 <= y3 <= y2)
                                                                    = y4, otherwise
|| If first line is horizontal and second line is vertical
get_intersection_coordinate (x1, y1) (x2, y1) (x3, y2) (x3, y3) = (x3, y1)
|| If first line is vertical and second line is horizontal
get_intersection_coordinate (x1, y1) (x1, y2) (x2, y3) (x3, y3) = (x1, y3)



point_exists (x1, y) (a1, y) (a2, y) = or [(a1 <= x1 <= a2), (a2 <= x1 <= a1)]
point_exists (x, y1) (x, b1) (x, b2) = or [(b1 <= y1 <= b2), (b2 <= y1 <= b1)]
point_exists (x, y) c d = False

find_intersections from to = foldl op [] from
                             where
                             op found (c1, c2) = found ++ points
                                                 where
                                                 intersection_exists (c3, c4) = intersection_possible c1 c2 c3 c4
                                                 coordinates_fn (c3, c4) = get_intersection_coordinate c1 c2 c3 c4
                                                 matches = filter intersection_exists to
                                                 points = map coordinates_fn matches


create_line_segments_with_steps :: [((num, num), (num, num))] -> [((num, num), (num, num), num)]
create_line_segments_with_steps ((c1, c2):line_segments) = foldl op initial line_segments
                                                           where
                                                           initial = [(c1, c2, manhattan_distance c1 c2)]
                                                           op result (c3, c4) = result ++ [(c3, c4, z)]
                                                                                where
                                                                                (px, py, pz) = last result
                                                                                z = (pz + (manhattan_distance c3 c4))

steps_to_intercept list_of_line_segments_with_steps (x, y) = result
                                                             where
                                                             not_reached_intersection (c1,c2,i) = (equal (point_exists (x, y) c1 c2) False)
                                                             get_count path = (x, y, steps + (manhattan_distance c2 (x, y)))
                                                                              where
                                                                              (c1, c2, steps) = last path
                                                             paths = map (takewhile not_reached_intersection) list_of_line_segments_with_steps
                                                             result = (map get_count paths)



main = (show result)
        where
        first_wire_coordinates = generate_coordinates first_wire
        second_wire_coordinates = generate_coordinates second_wire
        first_wire_line_segments = generate_line_segments first_wire_coordinates
        second_wire_line_segments = generate_line_segments second_wire_coordinates
        intersections = tl (mkset ((find_intersections first_wire_line_segments second_wire_line_segments) ++ (find_intersections second_wire_line_segments first_wire_line_segments)))
        wire1_with_steps = create_line_segments_with_steps first_wire_line_segments
        wire2_with_steps = create_line_segments_with_steps second_wire_line_segments

        path_counts = map (steps_to_intercept ([wire1_with_steps, wire2_with_steps])) intersections
        get_sum_of_intercept path_list = result
                                         where
                                         result = foldl op 0 path_list
                                                  where
                                                  op count (i1, i2, c) = count + c
        sums = map get_sum_of_intercept path_counts
        result = sort sums

