#!/usr/bin/mira -exec

%include "../utils"

number_to_digits number = map to_digit (shownum number)
digits_to_number digit_list = numval (concat (map shownum digit_list))

ensure_code number = result
                     where
                     digits = number_to_digits number
                     first_number = hd digits
                     remaining_numbers = tl digits
                     op prev_list current_num = prev_list ++ [current_num], if last_el < current_num
                                              = front ++ [last_el] ++ [last_el], otherwise
                                                where
                                                last_el = last prev_list
                                                front = init prev_list

                     next_digits = foldl op [first_number] remaining_numbers
                     result = digits_to_number next_digits


count_codes number = (10 - (number mod 10)), if contains_adjacent_digits (digits_to_number front)
                   = 1, otherwise
                     where
                     digits = number_to_digits number
                     front = init digits

count_codes2 number = 1

contains_adjacent_digits number = result
                                  where
                                  number_as_digits = number_to_digits number
                                  op (x:y:xs) = equal x y, if equal (#xs) 0
                                              = or [equal x y, op (y:xs)], otherwise
                                  result = op number_as_digits


contains_special_adjacency number = not_equal idx (-1)
                                    where
                                    histogram = build_duplicates number
                                    op (n, count) = equal count 2
                                    idx = find_index op histogram

build_duplicates number = result
                          where
                          digits = number_to_digits number
                          first = hd digits
                          rest = tl digits
                          op built cur = built ++ [(cur, 1)], if not_equal a cur
                                       = f ++ [(a, b + 1)], otherwise
                                         where
                                         f = init built
                                         (a, b) = last built
                          result = foldl op [(first, 1)] rest

codes_within_range from to = result
                             where
                             find_codes low_code high_code = 0, if low_code > high_code
                                                           = (count_codes2 low_code) + (find_codes (get_next_code2 low_code) high_code), otherwise
                             result = find_codes (get_next_code2 (ensure_code from)) to

get_next_code number = ensure_code (number + (10 - (number mod 10)))

get_next_code2 number = until contains_special_adjacency op (op number)
                        where 
                        op x = ensure_code (x + 1), if contains_special_adjacency (ensure_code(x + 1))
                             = (get_next_code x), otherwise


generate_all_codes from to = result
                             where
                             op x y = [], if x > y
                                    = [(x, count_codes2 x)] ++ (op (get_next_code2 x) y), otherwise
                             result = op (get_next_code2 (ensure_code from)) to

main = result
       where
       codes = codes_within_range 248345 746315
       result = (codes)
