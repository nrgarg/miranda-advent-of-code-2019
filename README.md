# Advent of code 2019 - Miranda

My solutions for the [advent of code 2019](https://adventofcode.com/2019)
problem set using the [Miranda](http://miranda.org.uk/) programming language
