calc_fuel mass = (entier (mass / 3) - 2)
calc_fuel2 mass = 0,                         if result <= 0
                = result + calc_fuel2 result,    otherwise
                  where
                  result = calc_fuel mass
                

input :: [num]
input = readvals "input.txt"

main = show (sum (map calc_fuel2 input))
