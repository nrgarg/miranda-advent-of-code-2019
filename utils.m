equal a a = True
equal a b = False

not_equal a a = False
not_equal a b = True

assert x y z = x, if equal x y
             = error ("Assertion failed. " ++ z), otherwise


split :: [char] -> char -> [[char]]
split [] ch = []
split (a:[]) b = [], if equal a b
               = [[a]], otherwise
split str b    = [word] ++ (split remaining b)
                 where
                 word = (takewhile (not_equal b) str)
                 remaining = drop (#word + 1) str

to_digit '0' = 0
to_digit '1' = 1
to_digit '2' = 2
to_digit '3' = 3
to_digit '4' = 4
to_digit '5' = 5
to_digit '6' = 6
to_digit '7' = 7
to_digit '8' = 8
to_digit '9' = 9

to_number :: [char] -> num
to_number [] = 0
to_number digits = foldl op 0 digits
                   where
                   op a b = a * 10 + (to_digit b)

inject list 0 value        = value:(drop 1 list)
inject list position value = (take position list) ++ [value] ++ (drop (position + 1) list)


increment x = x + 1
decrement x = x - 1


find_index predicate list = result, if result < size
                          = -1, otherwise
                            where
                            result = until pred increment 0
                            size = #list
                            pred idx = (predicate (list!idx)), if idx < size
                                     = True, otherwise



